require('dotenv').config()
const moment = require('moment');
const { documentToHtmlString } = require('@contentful/rich-text-html-renderer');
const stripe = require('stripe')('sk_test_51HF9lvA3muMdLykvseB8485p8pcFShfxyBvKAfu3rDx4Z0t5WoWZWe0h1Du3LJLHFX4MtgF2Q9s3MNmY4MsluLI600qmEHQekw');

/*  contentful client */
const contentful = require('contentful')
const client = contentful.createClient({
  // host: "preview.contentful.com",
  space: process.env.CTFL_SPACE,
  accessToken: process.env.CTFL_ACCESSTOKEN
});

/* format date */
moment.locale('pt-br');

module.exports = function(eleventyConfig) {
  
  eleventyConfig.addPassthroughCopy("public");

  /* format date */
  eleventyConfig.addFilter('dateIso', date => {
    return moment(date).toISOString();
  })
  eleventyConfig.addFilter('dateReadable', date => {
    return moment(date).utc().format('ll'); // E.g. May 31, 2019
  })
  /* format price */
  eleventyConfig.addFilter('priceFormat', price => {
    return Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  })

  /* stripe prod - stripe price - contentful prod */
  eleventyConfig.addShortcode('handleProduct', async (contProd) => {
    const stripeProds = await stripe.products.list();
    const prices = await stripe.prices.list();
    const contentMatchStripeProd = stripeProds.data.filter( stripeProd => 
      stripeProd.name === contProd)
    const firstMatch = contentMatchStripeProd[0].id
    // console.log('first match', firstMatch);
    // console.log('result', contentMatchStripeProd)
    // console.log('prices', prices)
    const StripeProdMatchStripePrice = prices.data.filter( stripePrice => 
      stripePrice.product === firstMatch);
    // console.log('price', StripeProdMatchStripePrice)
    // console.log('price_id', StripeProdMatchStripePrice[0].id)
    const secondMatch = StripeProdMatchStripePrice[0].id
    return secondMatch;
  })

  /* handle RichText from Contentful */
  eleventyConfig.addShortcode('documentToHtmlString', documentToHtmlString);

  eleventyConfig.addShortcode('richText', (richText) => {
    const rawRichTextField = richText;
    
    const options = {
      renderNode:
      {
        'embedded-asset-block': ({ data: { target: { fields }}}) => {
          return `<br>
          <img src="${fields.file.url}"
            class="post-img"
            height="${fields.file.details.image.height}"
            width="${fields.file.details.image.width}"
            alt="${fields.description}"
          />
          <br><br>`
        },

        'paragraph': (node, next) => {
          return `<p>${next(node.content)
          // .replace(/\n/g, '<br/>')
         }<br></p>`
        }
      }
    }
    return documentToHtmlString(rawRichTextField, options).replace(/\u00a0/g, " ");
  });

  // eleventyConfig.addCollection("postsReversed", collection => {
  //   return collection.getFilteredByTag("post").reverse();
  // });
  
  return {
    passthroughFileCopy: true,
    markdownTemplateEngine: "njk",
    templateFormats: ["html", "njk", "md"],
    dir: {
      input: "src",
      output: "_site",
      includes: "_includes"
    }
  }
}