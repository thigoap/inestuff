const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

module.exports = async () => {
  const prices = await stripe.prices.list();
  return prices.data;
}
