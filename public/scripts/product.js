const imgSlider = (selectedImg) => {
  document.getElementById('mainImg').src = selectedImg;
}

const mainImg = document.getElementById('mainImg')
const closeModal = document.getElementById('closeModal')
const modal = document.getElementById('modal-container')

mainImg.addEventListener('click', () => {
  document.getElementById('modalImg').src = mainImg.src;
  modal.classList.add('show');
})
closeModal.addEventListener('click', () => {
  modal.classList.remove('show');
})


/* stripe payment */
const stripe = Stripe('pk_test_51HF9lvA3muMdLykvVcoqUPe2134WEb1WRxtnip1SgjXGmplmUdctqeX1lLnXPu3vRbYwa9rbfrpOsZsw61dHVhmq00rq7IC0QT')

const checkoutButton = document.getElementById('checkout-button');

checkoutButton.addEventListener('click', () => {
  const price_id = document.getElementById('price_id').textContent;
  // When the customer clicks on the button, redirect
  // them to Checkout.
  stripe.redirectToCheckout({
    // lineItems: [{price: 'price_1HFAo3A3muMdLykvWC9Zjn95', quantity: 1}],
    lineItems: [{price: String(price_id.trim()), quantity: 1}],
    mode: 'payment',
    // Do not rely on the redirect to the successUrl for fulfilling
    // purchases, customers may not always reach the success_url after
    // a successful payment.
    // Instead use one of the strategies described in
    // https://stripe.com/docs/payments/checkout/fulfillment
    successUrl: 'http://localhost:8080/sucesso',
    cancelUrl: 'http://localhost:8080/loja/',
  })
  .then(function (result) {
    if (result.error) {
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer.
      var displayError = document.getElementById('error-message');
      displayError.textContent = result.error.message;
    }
  });
});
