const menuBtn = document.getElementById('menuBtn')
const closeBtn = document.getElementById('closeBtn')
const sideMenuContainer = document.getElementById('sideMenuContainer')

menuBtn.addEventListener('click', () => {
  sideMenuContainer.classList.add('show');
  menuBtn.classList.add('hide');
})
closeBtn.addEventListener('click', () => {
  sideMenuContainer.classList.remove('show');
  menuBtn.classList.remove('hide');
})

window.onload = function() {
  
  function freshDot(){
    this.obj = document.createElement("div");
    this.obj.classList.add("box");
    this.obj.style.top = (window.innerHeight * Math.random()) + 'px';
    this.obj.style.left = (window.innerWidth * Math.random()) + 'px';
    let colorR = Math.floor(Math.random() * 255); 
    let colorG = Math.floor(Math.random() * 255); 
    let colorB = Math.floor(Math.random() * 255); 
    this.obj.style.background = "rgb(" + colorR + "," + colorG + "," + colorB + ")";
    this.size = Math.floor(5 * Math.random()) + 7;
    this.obj.style.height =  this.size + 'px';
    this.obj.style.width = this.size + 'px';
    document.body.appendChild(this.obj);
  }

  let dot = [];
  for(let i = 0 ; i < 50 ; i++ ){
    dot.push(new freshDot());
  }

/*   const counter = document.getElementById('counter');
  function updateVisitCount() {
    fetch('https://countapi.xyz/hit/inestuff/115a1d59-b57e-41cb-9321-3c2d5e71081b')
      .then(res => res.json())
      .then(counter.innerHTML = res.value);
  }
  updateVisitCount() */

  // exemplo instagram
/*   fetch('https://graph.instagram.com/17907873844245221?fields=id,media_type,media_url,username,timestamp&access_token=IGQVJYVlNsWWU2SFhPNkNoVUJBWW5YeWtwX2VCYWY4a3pBeFhVMjljQjNxZA2hEZAFZAoS19TZAVlpNkJtS3lKQldMOVZAETlNwbXZAqRlltNWpNaHdNUmRwMG51ZAmFwT1cxZAUJoLXRObE80TDFQZA2V2WnJaVmI5T1JORlZA4YTRj')
  .then(res=> res.json())
  .then(result => document.getElementById('testeimg').src=result.media_url)
} */
  /*
  $(window).resize(function(){
    for(i=0;i<50;i++){
      document.body.removeChild(dot[i]);
    }
  });*/
};
